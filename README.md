# Processo de compilação do Kernel do Linux

---

## Introdução

Documento desenvolvido como atividade para a disciplina *Sistemas Operacionais* do *Tecnólogo em Análise e Desenvolvimento de Sistemas* ministrada no IFPE - Campus Recife pelo Prof. Anderson Moreira.

**Equipe:**

- Denis Moura
- Sérgio Adriani
- Valter Negreiros
- Raylson Andrade

## Pré-Requisitos

A compilação do kernel foi realizada em uma Virtual Machine, com 2 GB de memória RAM e 50 GB de armazenamento, rodando a versão 16.2 do Linux Mint Cinamon.

Recomenda-se que os testes sejam realizados em uma virtual machine por questões de segurança. Caso algo não ocorra como esperado, há a possibilidade de começar todo processo do início em um novo ambiente sem comprometer a integridade de qualquer arquivo importante.

Até a data deste tutorial, a versão mais estável do kernel do linux é a 5.2.11. Conforme a data, está versão pode variar.

## Preparando o Ambiente

### Baixando os arquivos necessários

Acesse o site www.kernel.org clicando [aqui](www.kernel.org)

![text](imagens-kernel-linux\kernel-org.png)

Baixe a versão mais estável do kernel do linux clicando no botão que está escrito *Latest Stable Kernel: 5.2.11*

![text](imagens-kernel-linux\kernel-org2.png)

Após a conclusão do dowloand, abra o terminal e entre no modo de super usuário utilizando o comando abaixo. Será necessário utilizar a senha do usuário quando solicitado.

```bash
sudo su
```

![text](imagens-kernel-linux\sudo-su.png)

Acesse a pasta *Dowloands* e copie o arquivo baixado para a pasta */tmp* usando o comando:

```bash
cp linux-5.2.11.tar.xz /tmp
```

![text](imagens-kernel-linux\capturar.png)

Acesse a pasta tmp e descompacte o arquivo *linux-5.2.11.tar.xz* usando o comando:

```bash
tar xvf linux-5.2.11.tar.xz
```

![text](imagens-kernel-linux\capturar2.png)

Uma pasta será gerada com o nome *linux-5.2.11*.

![text](imagens-kernel-linux\capturar3.png)

### Instalando Dependências

Antes de realizar a compilação é necessário instalar algumas bibliotecas e compiladores utilizados no processo.

Primeiro, iremos instalar um pacote de compiladores chamado build-essential através do comando:

```bash
apt install build-essential -y
```

![text](imagens-kernel-linux\capturar4.png)

Precisamos instalar também duas bibliotecas utilizadas no processo de geração do arquivo *.config* : [Colocar o nome das libs aqui]. Utilize o código:

```bash
apt install libssl-dev -y && apt install libncurses-dev -y
```

![text](imagens-kernel-linux\capturar5.png)

### Gerando o arquivo .config

Existem diversos comando que podem ser utilizados para gerar o .config, porém utilizaremos o nconfig. Esse comando é um *alias* contido no makefile da pasta.

Acesse a pasta *linux-5.2.11* e utilize o seguinte comando para gerar o arquivo *.config*:

```bash
make nconfig
```

Ao final da execução, será aberta uma área de configuração. Nela, você poderá selecionar ou desmarcar elementos que podem ser compilados. Há uma marcação default e não iremos alterá-la. Para dar continuidade ao processo, aperte *F6*.

![text](imagens-kernel-linux\capturar6-1.png)

Confirme o nome do arquivo clicando na *tecla enter* e ao final, aperte *F9* para sair.

![text](imagens-kernel-linux\capturar7.png)

Para confirmar se o arquivo foi criado com sucesso, liste todos os arquivos ocultos da pasta usando o camando abaixo e verifique se o arquivo .config se encontra na pasta.

```bash
list -la
```

Nesta etapa, já estamos haptos a realizar a compilação do kernel.

## Compilando o Kernel

A compilação do kernel pode ser feita de forma paralela. Para isso, inicialmente precisamos descobrir quantos  quantos núcleos há disponíveis para utilização. Utilize o comando:

```bash
nproc
```

Se desejar realizar a compilação multi-core, utilize o camando abaixo substituindo *\[numero-de-núcleos\]* pelo numero de núcleos que deseja utilizar

```bash
make -j[numero-de-núcleos]
```

![text](imagens-kernel-linux\capturar8.png)

Caso queria relizar a compilação mono-core, digite o camando abaixo, ignorando o parâmetro *-j[numero-de-núcleos]*

```bash
make
```

Aguarde. O processo é um pouco demorado.
